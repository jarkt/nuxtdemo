.PHONY: network_create network_clean

network_create:
	$(MSG_INFO_START)Create Network...$(MSG_END)
	@-docker network create $(PROJECT_NAME)

network_remove:
	$(MSG_INFO_START)Remove Network...$(MSG_END)
	@-docker network rm $(PROJECT_NAME)
