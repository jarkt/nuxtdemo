.PHONY: ngrok_url

ngrok_url:
	@-docker exec -it nuxtdemo_frontend_1 /bin/bash -c /scripts/ngrok_url.sh
