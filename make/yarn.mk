.PHONY: yarn yarn_install

yarn:
	@-docker exec -it nuxtdemo_frontend_1 yarn $(CMD)
ifeq ($(CMD),)
	$(MSG_INFO_START)make yarn CMD=$(MSG_END)
endif

yarn_install:
	$(MSG_INFO_START)Yarn install...$(MSG_END)
	@-docker exec -it nuxtdemo_frontend_1 yarn install$(CMD)
