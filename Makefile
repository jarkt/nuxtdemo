include .env

SHELL := /bin/bash
WORKDIR := $(shell (pwd | sed "s/^\/mnt//"))
MSG_PROGRESS_START := @printf "\033[1;96m
MSG_INFO_START := @printf "\n\033[1;33m
MSG_SUCCESS_START := @printf "\n\033[0;32m
MSG_END := \033[0m\n\n"
PROJECT_VOLUME := project-data
PROJECT_VOLUME_NAME := $(PROJECT_NAME)_project-data
DOCKER_BUILDKIT := 1

.PHONY: setup build create start stop kill remove clean

setup: build create start sftp_upload yarn_install sftp_download
	$(MSG_SUCCESS_START)Setup completed.$(MSG_END)

build: frontend_dev_build stack_build
	$(MSG_SUCCESS_START)Build completed.$(MSG_END)

create: network_create volume_create stack_create sftp_create
	$(MSG_SUCCESS_START)Create completed.$(MSG_END)

start: stack_start sftp_start
	$(MSG_SUCCESS_START)Start completed.$(MSG_END)

stop: stack_stop sftp_stop
	$(MSG_SUCCESS_START)Stop completed.$(MSG_END)

kill: stack_kill sftp_kill
	$(MSG_SUCCESS_START)Kill completed.$(MSG_END)

remove: stack_remove sftp_remove network_remove
	$(MSG_SUCCESS_START)Remove completed.$(MSG_END)

clean: stack_clean tools_clean sftp_clean volume_clean
	$(MSG_SUCCESS_START)Clean completed.$(MSG_END)

include make/frontend_dev.mk
include make/network.mk
include make/ngrok.mk
include make/sftp.mk
include make/stack.mk
include make/volume.mk
include make/yarn.mk
