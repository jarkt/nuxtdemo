.PHONY: volume_create volume_clean

volume_create:
	$(MSG_INFO_START)Create project data volume...$(MSG_END)
	@-docker volume create $(PROJECT_VOLUME_NAME)

volume_clean:
	$(MSG_INFO_START)Remove project data volume...$(MSG_END)
	@-docker volume rm $(PROJECT_VOLUME_NAME)
