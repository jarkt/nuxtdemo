.PHONY: sftp_create sftp_start sftp_stop sftp_kill sftp_remove sftp_clean

sftp_create:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Create SFTP container...$(MSG_END)
	$(eval USERS := -e SFTP_USERS=$(SFTP_USER):$(SFTP_PASSWD):33::upload)
	$(eval VOLUME := -v $(PROJECT_VOLUME_NAME):/home/$(SFTP_USER)/upload/)
	@-docker create --name $(PROJECT_NAME)_sftp $(USERS) $(VOLUME) -p $(SFTP_PORT):22 --entrypoint "" atmoz/sftp \
		/bin/bash -c "/entrypoint chown $(SFTP_USER) /home/$(SFTP_USER)/upload && /entrypoint"
endif

sftp_start:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Start SFTP container...$(MSG_END)
	@-docker start $(PROJECT_NAME)_sftp
endif

sftp_stop:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Stop SFTP container...$(MSG_END)
	@-docker stop $(PROJECT_NAME)_sftp
endif

sftp_kill:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Kill SFTP container...$(MSG_END)
	@-docker kill $(PROJECT_NAME)_sftp
endif

sftp_remove:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Remove SFTP container...$(MSG_END)
	@-docker rm $(PROJECT_NAME)_sftp
endif

sftp_clean:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Remove SFTP image...$(MSG_END)
	@-docker rmi atmoz/sftp
endif

sftp_upload:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Upload project to SFTP...$(MSG_END)

	$(MSG_PROGRESS_START)Create tar...$(MSG_END)
	@-tar --owner=33 --group=33 -zcf /tmp/project.tar.gz .

	$(MSG_PROGRESS_START)Copy tar...$(MSG_END)
	@-docker cp /tmp/project.tar.gz $(PROJECT_NAME)_sftp:/tmp

	$(MSG_PROGRESS_START)Cleanup target folder...$(MSG_END)
	$(eval PROJECT_PATH := /home/$(SFTP_USER)/upload)
	@-docker exec -it nuxtdemo_sftp /bin/sh -c " \
		rm -rf $(PROJECT_PATH)/{*,.*} \
	"

	$(MSG_PROGRESS_START)Extract tar...$(MSG_END)
	@-docker exec -it nuxtdemo_sftp /bin/sh -c " \
		tar -xzf /tmp/project.tar.gz -C $(PROJECT_PATH)/ \
	"

	$(MSG_PROGRESS_START)Delete remote tar...$(MSG_END)
	@-docker exec -it nuxtdemo_sftp /bin/sh -c " \
		rm /tmp/project.tar.gz \
	"

	$(MSG_PROGRESS_START)Delete local tar...$(MSG_END)
	@-rm /tmp/project.tar.gz
endif

sftp_download:
ifeq ($(USE_SFTP), true)
	$(MSG_INFO_START)Download frontend from SFTP...$(MSG_END)

	$(MSG_PROGRESS_START)Create tar...$(MSG_END)
	$(eval PROJECT_PATH := /home/$(SFTP_USER)/upload)
	@-docker exec -it nuxtdemo_sftp /bin/sh -c " \
		tar --owner=33 --group=33 -zcf /tmp/frontend.tar.gz -C $(PROJECT_PATH)/frontend . \
	"

	$(MSG_PROGRESS_START)Copy tar...$(MSG_END)
	@-docker cp $(PROJECT_NAME)_sftp:/tmp/frontend.tar.gz /tmp/

	$(MSG_PROGRESS_START)Cleanup target folders...$(MSG_END)
	@-rm -rf frontend && mkdir frontend

	$(MSG_PROGRESS_START)Extract tar...$(MSG_END)
	@-tar -xzf /tmp/frontend.tar.gz -C frontend/

	$(MSG_PROGRESS_START)Delete local tar...$(MSG_END)
	@-rm /tmp/frontend.tar.gz

	$(MSG_PROGRESS_START)Delete remote tar...$(MSG_END)
	@-docker exec -it nuxtdemo_sftp /bin/sh -c " \
		rm /tmp/frontend.tar.gz \
	"
endif
