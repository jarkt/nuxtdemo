.PHONY: stack_build stack_create stack_start stack_stop stack_kill stack_remove stack_clean set_compose_vars

stack_build: set_compose_vars
	$(MSG_INFO_START)Build stack images...$(MSG_END)
	@-$(COMPOSE_CALL) build

stack_create: set_compose_vars
	$(MSG_INFO_START)Create stack containers...$(MSG_END)
	@-$(COMPOSE_CALL) up --no-start

stack_start: set_compose_vars
	$(MSG_INFO_START)Start stack...$(MSG_END)
	@-$(COMPOSE_CALL) up -d --no-build --no-recreate

stack_stop: set_compose_vars
	$(MSG_INFO_START)Stop stack...$(MSG_END)
	@-$(COMPOSE_CALL) stop

stack_kill: set_compose_vars
	$(MSG_INFO_START)Kill stack...$(MSG_END)
	@-$(COMPOSE_CALL) kill

stack_remove: set_compose_vars
	$(MSG_INFO_START)Remove stack...$(MSG_END)
	@-$(COMPOSE_CALL) down --remove-orphans

stack_clean: set_compose_vars
	$(MSG_INFO_START)Remove stack volumes and images...$(MSG_END)
	@-$(COMPOSE_CALL) down --rmi all --volumes

# For internal use:
set_compose_vars:
ifneq ($(USE_SFTP), true)
	$(eval PROJECT_VOLUME := ${WORKDIR})
endif
	$(eval COMPOSE_VARS := \
		WORKDIR=$(WORKDIR) \
		PROJECT_VOLUME=$(PROJECT_VOLUME) \
		PROJECT_VOLUME_NAME=$(PROJECT_VOLUME_NAME) \
		COMPOSE_PROJECT_NAME=$(PROJECT_NAME) \
		HOSTNAME=$(HOSTNAME) \
	)
	$(eval COMPOSE_CALL := env $(COMPOSE_VARS) docker-compose -f docker/docker-compose.yml)
