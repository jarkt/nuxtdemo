.PHONY: frontend_dev_build

frontend_dev_build:
	$(MSG_INFO_START)Build frontend_dev image...$(MSG_END)
	@-docker build \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		-t $(PROJECT_NAME)/frontend_dev:latest \
		./docker/frontend_dev/

frontend_dev_lintfix:
	$(MSG_INFO_START)Fix JS Code...$(MSG_END)
	@-docker exec -it nuxtdemo_frontend_1 /bin/sh -c " \
		yarn run lintfix \
	"
ifeq ($(USE_SFTP), true)
	$(MSG_SUCCESS_START)Download changes with: make sftp_download$(MSG_END)
endif
